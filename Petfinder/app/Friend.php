<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    public function pets()
    {
        return $this->belongsToMany(Pet::class, 'pets_friends', 'friend_id', 'pet_id')->withTimestamps();
    }
}
