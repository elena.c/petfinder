<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pet;


class PetsFilterController extends Controller
{
    public function filter(Request $request){

        $filter = [];
        
        $checkbox = [];

        foreach ($request->all() as $k => $v){
            if(is_array($v)){
                foreach ($v as $key => $goodwith){
                    $checkbox[$key] = $goodwith; 
                }
            }
            if($v != -1 && $v != 'all' && !is_array($v)) {
                $filter[$k] = $v;
            }    
        }

        if (count($checkbox) != 0){
            $pets = Pet::where($filter)->whereHas('friends', function($query) use($checkbox){             
                $query->whereIn('friend_id', $checkbox);
            })->get();
            return view('includes.filterPets', compact('pets'))->render();
        }

        $pets = Pet::where($filter)->get();
        return view('includes.filterPets', compact('pets'))->render();
    }
}
