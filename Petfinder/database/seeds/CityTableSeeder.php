<?php

use Illuminate\Database\Seeder;

use App\City;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $city = [
            'Берово' => 2330,
            'Битола' => 7000,
            'Валандово'=> 2460,
            'Велес' => 1400,
            'Виница' => 2310,
            'Гевгелија' => 1480,
            'Гостивар' => 1230,
            'Дебар'=> 1250,
            'Делчево' => 2320,
            'Демир Капија' => 1442,
            'Демир Хисар' => 7240,
            'Кавадарци'=> 1430,
            'Кичево' => 6250,
            'Кочани' => 2300,
            'Кратово' => 1360,
            'Крива Паланка' => 1330,
            'Крушево'=> 7550,
            'Куманово' => 1300,
            'Македонски Брод' => 6530,
            'Македонска Каменица' =>2304,
            'Неготино' => 1440,
            'Охрид' => 6000,
            'Пехчево' => 2326,
            'Прилеп' => 7500,
            'Пробиштип' => 2210,
            'Радовиш' => 2420,
            'Ресен'=> 7310,
            'Свети Николе' => 2220,
            'Скопје' => 1000,
            'Струга'=> 6330,
            'Струмица' => 2400 ,
            'Тетово' => 1200,
            'Штип' => 2000];


       foreach ($city as $name => $zip_code) {
           $city = new City;
           $city->name = $name;
           $city->zip_code = $zip_code;
           $city->save();
       }
    }
}
