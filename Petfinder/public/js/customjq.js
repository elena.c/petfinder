$(function () {

//Превенција од отварање на слика во browser без да се аплоадира

    window.addEventListener("dragover",function(e){
    e = e || event;
    e.preventDefault();
    },false);

    window.addEventListener("drop",function(e){
    e = e || event;
    e.preventDefault();
    },false);

//Логика за вртење на стрелчето од менито
    $('#button').click(function() {    
        $("#strelce").toggleClass("fa-angle-up fa-angle-down"); 
    });

//Логика за прикажување на user profile shortcut
    $('#user_avatar').click(function() {    
        $("#user_links").slideToggle(); 
        if($('#user_avatar').hasClass('reverse-color')){
            $('#user_avatar').removeClass('reverse-color');
        } else {
            $('#user_avatar').addClass('reverse-color');
        }
    });

//Логика за да се прикажува футерот на скрол најдолу на екран
    $(window).on("scroll", function() {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
            $('footer').removeClass('nav-down').addClass('nav-up');
        }
        else {
            $('footer').removeClass('nav-up').addClass('nav-down');
        }
    });

    
});