Dropzone.options.myDropzone= {
    url: '/images-save',
    autoProcessQueue: false,
    uploadMultiple: true,
    parallelUploads: 100,
    maxFiles: 10,
    maxFilesize: 50,
    dictRemoveFile: 'Remove file',
    dictFileTooBig: 'Image is larger than 50MB',
    addRemoveLinks: true,
    init: function() {
        dzClosure = this; // Makes sure that 'this' is understood inside the functions below.

        // for Dropzone to process the queue (instead of default form behavior):
        document.getElementById("submit-all").addEventListener("click", function(e) {
            // Make sure that the form isn't actually being sent.
            e.preventDefault();
            e.stopPropagation();
            dzClosure.processQueue();
        });

        //send all the form data along with the files:
        this.on("sendingmultiple", function(data, xhr, formData) {
            var token = $("input[name='_token']").val();
            formData.append('_token', token);
            formData.append("pet_id", $("#pet_id").val());
        });

        this.on("maxfilesexceeded", function(file){
            alert("Можете да прикачите најмногу 10 фотографии");
        });
    }
}