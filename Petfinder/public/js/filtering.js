$(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $(document).on('change', '.formFilter :input', function(){
        var data_save = $('.formFilter').serializeArray();

        $(document).on('click', '.filter-form-close', function(){

            var data_save = $('.formFilter').serializeArray();
            
            ajaxFilter(data_save);
            
        });

        ajaxFilter(data_save);
    });

    function ajaxFilter(data) {
        $.ajax({
            type: "GET",
            url: '/filter',
            dataType: 'html',
            data: data    
            
          }).done(function (data) {
            $('.all-pets').empty();
            $('.all-pets').html(data);                
          }).fail(function (error) {
                console.log('error')
        });
    }

});