$(function(){

    $(document).on('click', '#inquire', function(){
        var firstName = $('.mail-content-div').find("input[name=first_name]").val();
        var lastName = $('.mail-content-div').find("input[name=last_name]").val();
        var telephone = $('.mail-content-div').find("input[name=telephone]").val();
        var email = $('.mail-content-div').find("input[name=email]").val();
        var cityID = $('.mail-content-div').find("select[name=city_id]").val();
        var message = $('.mail-content-div').find("textarea[name=message]").val();
        var petOwnerID = $('.mail-content-div').find("input[name=pet_owner_id]").val();
        var petID = $('.mail-content-div').find("input[name=pet_id]").val();

        $.ajax({
            type: "POST",
            url: '/inquirepet',
            data: {
                firstName: firstName,
                lastName: lastName,
                telephone: telephone,
                email: email,
                cityID: cityID,
                message: message,
                petOwnerID: petOwnerID,
                petID: petID
            }
        }).done(function(data){
            $('textarea').filter('[name=message]').val('');
            alert('Успешно пратена порака');
        }).fail(function(error){
            console.log('error');
        });
    })

});