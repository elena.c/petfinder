<!DOCTYPE html>
<html>
<head>
    @include('includes.headAdmin')
</head>
<body> 
    @section('content')     
    @show
    <nav class="navbar" id="home">
    <div class="container-fluid">
        <ul class="nav navbar-nav custom-navbar">
            <a class="navbar-brand logo-mobile" href="/">Brainster Project</a>
        </ul>

        <ul class="nav navbar-nav navbar-right custom-navbar">
            @guest
            <li class="max-height"><a class="nav-link modal-opener" href="" data-toggle="modal" data-target="#Login_Modal"><i class="nav-icon fas fa-user"></i> Најави се / Регистрирај се</a></li>
            @else
            <li class="nav-item">
                <a id="user_avatar" class="nav-link nav-link-username reverse-color" href="#"><i class="nav-icon fas fa-user userIcon-mobile"></i>
                    {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                </a>
                <div id="user_links">
                    <ul class="list-unstyled user-links-holder">
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Одјави ме') }}
                            </a>
                        </li>
                    </ul>
                        
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
            @endguest
        </ul>

    </div>
</nav>




    <div class="container-fluid petsForApprove">
        <div class="row">
            <div class="table-responsive">
            <table class="table">
                            <thead class="tableHead">
                                <tr>
                                    <th>Име</th>
                                    <th>Тип</th>
                                    <th>Име на корисник</th>
                                    <th>Град</th>
                                    <th>Години</th>
                                    <th>Големина</th>
                                    <th>Пол</th>
                                    <th>Опис</th>
                                    <th>Статус</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="table">
                                @foreach($pets as $pet)
                                  <tr>
                                    <td>{{ $pet->name }}</td>
                                    <td>{{ $pet->type}}</td>
                                    <td>{{ $pet->user->first_name }}</td>
                                    <td>{{ $pet->city->name }}</td>
                                    <td>{{ $pet->age }}</td>
                                    <td>{{ $pet->size }}</td>
                                    <td>{{ $pet->gender }}</td>
                                    <td>{{ $pet->description }}</td>
                                    <td>{{ $pet->approved }}</td>

                                    <td><a href="{{ route('update',['id' => $pet->id]) }}"class="btn btnStatus">Статус</a> </td>
                                  </tr>  

                                 @endforeach  
                        </tbody>
                        </table>
                        </div>

        </div>
    </div>
    <script>
        $(function () {

            $(window).on("scroll", function() {
                var scrollHeight = $(document).height();
                var scrollPosition = $(window).height() + $(window).scrollTop();
                if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
                    $('footer').removeClass('nav-down');
                }
                else {
                    $('footer').addClass('nav-down');
                }
            });

            $('#registration_modal_opener').click(function() {
                $('#Login_Modal').hide();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $('#Registration_Modal').modal('toggle');
                
            })

            $('#button').click(function() {    
                $("#strelce").toggleClass("fa-angle-up fa-angle-down"); 
            });

            $('#user_avatar').click(function() {    
                $("#user_links").slideToggle(); 
                if($('#user_avatar').hasClass('reverse-color')){
                    $('#user_avatar').removeClass('reverse-color');
                } else {
                    $('#user_avatar').addClass('reverse-color');
                }
            });
           
            

        });
    </script>

</body>
</html>
