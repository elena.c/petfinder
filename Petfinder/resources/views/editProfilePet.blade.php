@extends('master') 
@section('content')
<div class="container-fluid">
    <div class="add-a-pet-body">
        <div class="card">
            <div class="card-section card-section_constrainedPadLg">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-unstyled list-inline">
                            <li><a class="item-link" href="{{route('editProfileUser')}}">За мене</a></li>
                            <li><a class="item-link active" href="{{route('editProfilePet')}}">Моите миленичиња</a></li>
                            <li><a class="item-link" href="#addNewPet">Додади милениче</a></li>
                        </ul>
                        <div class="separator"></div>
                    </div>
                </div>

                <div class="row">
                    @foreach($pets as $pet)
                        <div class="col-md-4 col-sm-6 pet-card-element u-vr10x">
                            <div class="pet-card white-bg">
                                <div class="media">
                                    <a href="{{route('petID', ['id' => encrypt($pet->id)])}}">
                                        <div class="media-image">
                                            <img class="img-responsive card-image" src="{{asset('storage/'.$pet->uploads()->first()->filename)}}">
                                        </div>
                                    </a>
                                    <div class="white-clip">
                                    </div>
                                </div>
                                <div class="card-caption">
                                    <p class="pet-name">{{$pet->name}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <form enctype="multipart/form-data" method="post" action="{{route('savePet')}}">
                    {{csrf_field()}}
                    <div class="row t-vr10x" id="addNewPet">
                        <div class="col-md-6">
                            <label for="name">Име</label><br>
                            <input type="text" id="name" name="name" class="login-input{{ $errors->has('name') ? ' is-invalid' : '' }}"> @if($errors->has('name'))
                            <span class="red pull-right">{{$errors->first('name')}}</span> @endif
                        </div>

                        <div class="col-md-6">
                            <label for="type">Вид на милениче</label><br>
                            <select class="login-input{{ $errors->has('type') ? ' is-invalid' : '' }}" id="type" name="type">
                                <option selected disabled>Одбери...</option>
                                <option value="cat">Маче</option>
                                <option value="dog">Куче</option>
                            </select> @if($errors->has('age'))
                            <span class="red pull-right">{{$errors->first('type')}}</span> @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="city_id">Локација на која се наоѓа</label><br>
                            <select class="login-input{{ $errors->has('city_id') ? ' is-invalid' : '' }}" id="city_id" name="city_id">
                                <option selected disabled>Одбери...</option>
                                @foreach (\App\City::all() as $city)
                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select> @if($errors->has('city_id'))
                            <span class="red pull-right">{{$errors->first('city_id')}}</span> @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label for="age">Возраст</label><br>
                            <select class="login-input{{ $errors->has('age') ? ' is-invalid' : '' }}" id="age" name="age">
                                <option selected disabled>Одбери...</option>
                                <option value="young">Младо</option>
                                <option value="adult">Возрасно</option>
                                <option value="old">Старо</option>
                            </select> @if($errors->has('age'))
                            <span class="red pull-right">{{$errors->first('age')}}</span> @endif
                        </div>

                        <div class="col-md-8">
                            <label for="good_with">Добро се согласува со</label><br>
                            <div class="login-input login-input-list">
                                <input class="checkboxes" type="checkbox"  name="good_with[]" value=1>Мачки
                                <input class="checkboxes" type="checkbox"  name="good_with[]" value=2>Кучиња
                                <input class="checkboxes" type="checkbox"  name="good_with[]" value=3>Деца
                            </div>
                           @if($errors->has('good_with'))
                            <span class="red pull-right">{{$errors->first('good_with')}}</span> @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="size">Раст</label>
                            <select class="login-input{{ $errors->has('size') ? ' is-invalid' : '' }}" id="size" name="size">
                                <option selected disabled>Одбери...</option>
                                <option value="sm">Мал раст</option>
                                <option value="md">Среден раст</option>
                                <option value="lg">Голем раст</option>
                            </select> @if($errors->has('size'))
                            <span class="red pull-right">{{$errors->first('size')}}</span> @endif
                        </div>

                        <div class="col-md-6">
                            <label for="gender">Пол</label>
                            <select class="login-input{{ $errors->has('gender') ? ' is-invalid' : '' }}" id="gender" name="gender">
                                <option selected disabled>Одбери...</option>
                                <option value="m">Машки</option>
                                <option value="f">Женски</option>
                            </select> @if($errors->has('gender'))
                            <span class="red pull-right">{{$errors->first('gender')}}</span> @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="description">Опис</label>
                        <textarea class="login-input-textarea{{ $errors->has('description') ? ' is-invalid' : '' }}" id="description" name="description" rows="5" cols="20">{{old('description')}}</textarea>                            
                        @if($errors->has('description'))
                            <span class="red pull-right">{{$errors->first('description')}}</span> @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" name="submit" id="submit-all" value="Додади милениче" class="login-register-btn">
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection