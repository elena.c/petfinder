@extends('master') 
@section('content')
<div class="container-fluid">
    <div class="add-a-pet-body">
        <div class="card">
            <div class="card-section card-section_constrainedPadLg">

                <form method="post" action="{{route('editProfilePost')}}" autocomplete="off">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <ul class="list-unstyled list-inline">
                                <li><a class="item-link active" href="{{route('editProfileUser')}}">За мене</a></li>
                                <li><a class="item-link" href="{{route('editProfilePet')}}">Моите миленичиња</a></li>
                                <li><a class="item-link" href="{{route('editProfilePet')}}#addNewPet">Додади милениче</a></li>
                            </ul>
                            <div class="separator"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 register-margin">
                            <span class="btn-text ">Како се викаш?</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="text" class="login-input{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" placeholder="Име"
                                value={{\Auth::user() ? \Auth::user()->first_name : '' }}> @if($errors->has('first_name'))
                            <span class="red pull-right">{{$errors->first('first_name')}}</span> @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="text" class="login-input{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" placeholder="Презиме"
                                value={{\Auth::user() ? \Auth::user()->last_name : '' }}> @if($errors->has('last_name'))
                            <span class="red pull-right">{{$errors->first('last_name')}}</span> @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <span class="btn-text">Каде живееш?</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <select class="login-input{{ $errors->has('city_id') ? ' is-invalid' : '' }}" name="city_id">
                                @foreach (\App\City::all() as $city)
                                    <option {{ $city->id == \Auth::user()->city_id ? 'selected' : '' }} value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select> @if($errors->has('city_id'))
                            <span class="red pull-right">{{$errors->first('city_id')}}</span> @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <span class="btn-text">Како можеме да те исконтактираме?</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="text" class="login-input{{ $errors->has('telephone') ? ' is-invalid' : '' }}" name="telephone" placeholder="Телефонски број"
                                value="{{\Auth::user() ? \Auth::user()->telephone : ''}}"> @if($errors->has('telephone'))
                            <span class="red pull-right">{{$errors->first('telephone')}}</span> @endif
                        </div>
                        <div class="col-md-6">
                            <input disabled type="email" name="email" class="login-input{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Електронска пошта"
                                value="{{\Auth::user() ? \Auth::user()->email : ''}}"> @if($errors->has('email'))
                            <span class="red pull-right">{{$errors->first('email')}}</span> @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="password" name="password" class="login-input{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Лозинка">                            @if($errors->has('password'))
                            <span class="red pull-right">{{$errors->first('password')}}</span> @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="password" class="login-input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password_confirmation"
                                placeholder="Потврди ја лозинката"> @if($errors->has('password'))
                            <span class="red pull-right">{{$errors->first('password')}}</span> @endif
                        </div>
                    </div>

                    <input type="hidden" name="id" value="{{\Auth::user() ? \Auth::user()->id : ''}}">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="submit" class="login-register-btn" value="Сочувај">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection