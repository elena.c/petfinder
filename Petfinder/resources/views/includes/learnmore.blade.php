<div class="container-fluid">
    <div class="row white-bg-1">
        <div class="cover2-holder">
            <img class="cover2" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/06/hero-dog-checklist.jpg">
        </div>
        <h2 class="learn-more-heading text-center">Planning to adopt a pet?</h2>
    </div>
</div>
<div class="container-fluid">
    <div class="row  white-bg">
        <div class="col-md-4 learn-more-card">
            <div class="blue-image">
                <img class="blue-icon" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-18.png">
            </div>
            <div class="learn-more-caption">
                <p class="learn-more-caption-lg">Checklist for new adopters</p>
                <span>
                    Help make the transition as smooth as possible.
                </span>
                <button class="btn-clear btn-blue">
                    Learn more
                </button>
            </div>
        </div>
        <div class="col-md-4 learn-more-card">
            <div class="blue-image">
                <img class="blue-icon" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-17.png">
            </div>
            <div class="learn-more-caption">
                <p class="learn-more-caption-lg">Finding the right pet</p>
                <span>
                    Get tips on figuring out who you should adopt.
                </span>
                <button class="btn-clear btn-blue">
                    Learn more
                </button>
            </div>
        </div>
        <div class="col-md-4 learn-more-card">
            <div class="blue-image">
                <img class="blue-icon" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-8.png">
            </div>
            <div class="learn-more-caption">
                <p class="learn-more-caption-lg">About Brainster</p>
                <span>
                    Learn how to make awesome projects like this one.
                </span>
                <button class="btn-clear btn-blue">
                    Learn more
                </button>
            </div>
        </div>
    </div>
</div>