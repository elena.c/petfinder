<div id="Navbar_Modal" class="modal modal_card" role="dialog" aria-labelledby="Navbar_Modal_Header">

    <header class="mobileMenu-root-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-close fas fa-times"></i></button>
    </header>

    <div class="menuBody">
        <div class="mobileMenu-body">
            <ul class="mobileMenu-root-list" role="navigation">
                <li class="mobileMenu-sub-item"><a href="#" class="mobileMenu-sub-item-link">Се за вдомување на животни</a><i class="fas fa-angle-right"></i></li>
                <li class="mobileMenu-sub-item"><a href="#" class="mobileMenu-sub-item-link">Нега на кучиња</a><i class="fas fa-angle-right"></i></li>
                <li class="mobileMenu-sub-item"><a href="#" class="mobileMenu-sub-item-link">Нега на мачиња</a><i class="fas fa-angle-right"></i></li>
                <li class="mobileMenu-sub-item"><a href="#" class="mobileMenu-sub-item-link">Нега на животни</a><i class="fas fa-angle-right"></i></li>
                <li class="mobileMenu-sub-item"><a href="#" class="mobileMenu-sub-item-link">Засолништа & Спасување</a><i class="fas fa-angle-right"></i></li>
                <li class="mobileMenu-sub-item"><a href="#" class="mobileMenu-sub-item-link">Помош на животни</a><i class="fas fa-angle-right"></i></li>
                <li class="mobileMenu-sub-item"><a href="#" class="mobileMenu-sub-item-link">Видео</a><i class="fas fa-angle-right"></i></li>
                <li>
                    <form method="" action="">
                        <div class="fieldSearch">
                            <input type="text" name="" placeholder="Барај" class="field-input">
                            <button type="submit" class="field_icon-btn fas fa-search"></button>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        
    </div>
    <div class="mobilefooterLinks text-center">
    <a class="RegisterLink" href="{{route('login')}}"> Sign in / Register</a></li>
    </div>
</div>