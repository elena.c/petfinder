@extends('master') 
@section('content')
    @include('includes.cover') 

    {{-- MOBILE & TABLET --}}

<div class="container-fluid hidden-md hidden-lg">
    <div class="row">
        <div class="col-md-12 margin-top-mobile">
            <h2 class="text-centered">Миленичиња близу тебе</h2>
        </div>
        <div class="scrolling-wrapper">
            @foreach($pets as $pet)
            <div class="col-md-3 card pet-card-element u-vr10x">
                <div class="pet-card white-bg petFirstLayer">
                    <div class="media">
                        <a href="{{route('petID', ['id' => encrypt($pet->id)])}}">
                            <div class="media-image">
                                <img class="img-responsive card-image" src="{{asset('storage/'.$pet->uploads()->first()->filename)}}">
                            </div>
                        </a>
                        <div class="white-clip">
                        </div>
                    </div>
                    <div class="card-caption">
                        <p class="pet-name">{{$pet->name}}</p>
                    </div>
                </div>
                <div class="pet-card white-bg petSecondLayer">
                    <a href="{{route('petID', ['id' => encrypt($pet->id)])}}">
                        <div class="media">
                            <div class="media-image">
                                <img class="img-responsive card-image" src="{{asset('storage/photos/'.$pet->uploads()->first()->resized_name)}}">
                            </div>
                        </div>
                    </a>
                    <div class="white-circle click-share">
                        <i class="icon-share fas fa-share icon-share-close"></i>
                    </div>
                    <div class="card-caption">
                        <p class="second-layer-pet-name">{{$pet->name}}</p>
                        <ul class="list-unstyled pet-details detailsList">
                            <li>@if($pet->type == 'cat') Маче @elseif($pet->type == 'dog') Куче @endif
                            </li>
                            <li>
                                @if($pet->age == 'young') Младо @elseif($pet->age == 'adult') Возрасно @elseif($pet->age == 'old') Старо @endif &#9900 
                                @if($pet->gender == 'm') Машко @else Женско @endif
                            </li>
                            <li>{{$pet->city->name}}, Македонија</li>
                        </ul>
                        <p class="share-pet-name">
                            @if ($pet->gender == 'm') Сподели го @else Сподели ја @endif {{$pet->name}}
                        </p>
                        <ul class="list-unstyled list-inline shareList">
                            <li><i class="fab fa-facebook-f"></i></li>
                            <li><i class="fab fa-twitter"></i></li>
                            <li><i class="fab fa-pinterest"></i></li>
                            <li><i class="fas fa-envelope"></i></li>
                            <li><i class="fas fa-link"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="col-md-3 card u-vr10x">
                <div class="pet-card purple-card">
                    <div class="purple-media">
                        <div class="paw-image">
                            <span class="icon icon_xxl m-icon_colorWhite">
                                    <svg role="img" focusable="false" aria-hidden="true">
                                        <use xlink:href="#icon-strokedPaw">
                                            <svg id="icon-strokedPaw" viewBox="0 0 42.26 40.83" width="100%" height="100%"><title>icon-paw</title><path d="M10.42 17.57c0-3.51-2.34-6.36-5.21-6.36S0 14.06 0 17.57s2.34 6.35 5.21 6.35 5.21-2.85 5.21-6.35zm-5.21 4.57c-1.9 0-3.44-2.05-3.44-4.58s1.54-4.58 3.44-4.58 3.42 2.06 3.42 4.59-1.52 4.57-3.42 4.57zM42.26 17.56c0-3.5-2.34-6.35-5.21-6.35s-5.21 2.85-5.21 6.36a.89.89 0 1 0 1.77 0c0-2.52 1.54-4.58 3.44-4.58s3.44 2.05 3.44 4.58c0 2.52-1.54 4.57-3.44 4.57a.89.89 0 0 0 0 1.78c2.87 0 5.21-2.85 5.21-6.36zM27.63 12.72c2.87 0 5.21-2.85 5.21-6.36S30.49 0 27.63 0s-5.21 2.85-5.21 6.36 2.32 6.36 5.21 6.36zm0-10.94c1.9 0 3.44 2.05 3.44 4.58s-1.56 4.58-3.44 4.58-3.44-2.05-3.44-4.58 1.53-4.58 3.44-4.58zM14.63 12.72c2.87 0 5.21-2.85 5.21-6.36S17.52 0 14.63 0 9.42 2.85 9.42 6.36s2.35 6.36 5.21 6.36zm0-10.94c1.9 0 3.44 2.05 3.44 4.58s-1.53 4.58-3.44 4.58-3.44-2.05-3.44-4.58 1.56-4.58 3.44-4.58zM33.63 28.65l-5.5-9.63c-1.82-3.17-4.31-4.92-7-4.92s-5.18 1.75-7 4.92l-5.5 9.62a11.12 11.12 0 0 0-.9 2 7.54 7.54 0 0 0-.5 2.69.61.61 0 0 0 0 .08 7.5 7.5 0 0 0 7.1 7.41h.3a7.35 7.35 0 0 0 2.91-.59c.66-.26 1.76-.76 2.9-1.3l.41.17a18.59 18.59 0 0 0 7.07 1.71 7.51 7.51 0 0 0 7.14-7.39 9 9 0 0 0-.49-2.72 11.18 11.18 0 0 0-.94-2.05zm-5.72 10.39a15.1 15.1 0 0 1-5.28-1.14l.34-.17 1-.46a.86.86 0 0 0 .18-.14l1-.51.94-.48a.89.89 0 0 0-.8-1.59l-1 .48c-1.55.79-4.14 2.12-5.23 2.57l-.1.07c-.83.39-1.6.73-2.08.92a5.2 5.2 0 0 1-2.4.45h-.09a5.72 5.72 0 0 1-5.42-5.62.35.35 0 0 0 0-.07 5.82 5.82 0 0 1 .4-2.09 9.27 9.27 0 0 1 .77-1.7l5.49-9.65c1.49-2.6 3.43-4 5.47-4s4 1.43 5.47 4l5.53 9.63a9.47 9.47 0 0 1 .77 1.7 7.21 7.21 0 0 1 .42 2.18 5.71 5.71 0 0 1-5.38 5.62z"></path></svg>
                                        </use>
                                    </svg>
                                </span>
                        </div>
                        <span class="text-purple-media">281564 миленичиња<br> се достапни на Petfinder</span>
                    </div>
                    <div class="purple-caption">
                        <span class="btn-text white">Запознај ги</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



{{-- REGULAR --}}

<div class="container pet-card-border hidden-sm hidden-xs">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-centered margin-top">Кучиња за посвојување во Скопје</h2>
        </div>
    </div>
</div>

<div class="container-fluid pet-card-container hidden-sm hidden-xs">
    <div class="row">
        @foreach($dogs as $pet)
        <div class="col-md-3 pet-card-element u-vr10x">
            <div class="pet-card white-bg petFirstLayer">
                <div class="media">
                    <a href="{{route('petID', ['id' => encrypt($pet->id)])}}">
                        <div class="media-image">
                            <img class="img-responsive card-image" src="{{asset('storage/'.$pet->uploads()->first()->filename)}}">
                        </div>
                    </a>
                    <div class="white-clip">
                    </div>
                </div>
                <div class="card-caption">
                    <p class="pet-name">{{$pet->name}}</p>
                </div>
            </div>
            <div class="pet-card white-bg petSecondLayer">
                <a href="{{route('petID', ['id' => encrypt($pet->id)])}}">
                    <div class="media">
                        <div class="media-image">
                            <img class="img-responsive card-image" src="{{asset('storage/photos/'.$pet->uploads()->first()->resized_name)}}">
                        </div>
                    </div>
                </a>
                <div class="white-circle click-share">
                    <i class="icon-share fas fa-share icon-share-close"></i>
                </div>
                <div class="card-caption">
                    <p class="second-layer-pet-name">{{$pet->name}}</p>
                    <ul class="list-unstyled pet-details detailsList">
                        <li>@if($pet->type == 'cat') Маче @elseif($pet->type == 'dog') Куче @endif
                        </li>
                        <li>
                            @if($pet->age == 'young') Младо @elseif($pet->age == 'adult') Возрасно @elseif($pet->age == 'old') Старо @endif &#9900 
                            @if($pet->gender == 'm') Машко @else Женско @endif
                        </li>
                        <li>{{$pet->city->name}}, Македонија</li>
                    </ul>
                    <p class="share-pet-name">
                        @if ($pet->gender == 'm') Сподели го @else Сподели ја @endif {{$pet->name}}
                    </p>
                    <ul class="list-unstyled list-inline shareList">
                        <li><i class="fab fa-facebook-f"></i></li>
                        <li><i class="fab fa-twitter"></i></li>
                        <li><i class="fab fa-pinterest"></i></li>
                        <li><i class="fas fa-envelope"></i></li>
                        <li><i class="fas fa-link"></i></li>
                    </ul>
                </div>
            </div>
        </div>
        @endforeach

        <div class="col-md-3">
            <div class="pet-card purple-card">
                <div class="purple-media">
                    <div class="paw-image">
                        <span class="icon icon_xxl m-icon_colorWhite">
                                <svg role="img" focusable="false" aria-hidden="true">
                                    <use xlink:href="#icon-strokedPaw">
                                        <svg id="icon-strokedPaw" viewBox="0 0 42.26 40.83" width="100%" height="100%"><title>icon-paw</title><path d="M10.42 17.57c0-3.51-2.34-6.36-5.21-6.36S0 14.06 0 17.57s2.34 6.35 5.21 6.35 5.21-2.85 5.21-6.35zm-5.21 4.57c-1.9 0-3.44-2.05-3.44-4.58s1.54-4.58 3.44-4.58 3.42 2.06 3.42 4.59-1.52 4.57-3.42 4.57zM42.26 17.56c0-3.5-2.34-6.35-5.21-6.35s-5.21 2.85-5.21 6.36a.89.89 0 1 0 1.77 0c0-2.52 1.54-4.58 3.44-4.58s3.44 2.05 3.44 4.58c0 2.52-1.54 4.57-3.44 4.57a.89.89 0 0 0 0 1.78c2.87 0 5.21-2.85 5.21-6.36zM27.63 12.72c2.87 0 5.21-2.85 5.21-6.36S30.49 0 27.63 0s-5.21 2.85-5.21 6.36 2.32 6.36 5.21 6.36zm0-10.94c1.9 0 3.44 2.05 3.44 4.58s-1.56 4.58-3.44 4.58-3.44-2.05-3.44-4.58 1.53-4.58 3.44-4.58zM14.63 12.72c2.87 0 5.21-2.85 5.21-6.36S17.52 0 14.63 0 9.42 2.85 9.42 6.36s2.35 6.36 5.21 6.36zm0-10.94c1.9 0 3.44 2.05 3.44 4.58s-1.53 4.58-3.44 4.58-3.44-2.05-3.44-4.58 1.56-4.58 3.44-4.58zM33.63 28.65l-5.5-9.63c-1.82-3.17-4.31-4.92-7-4.92s-5.18 1.75-7 4.92l-5.5 9.62a11.12 11.12 0 0 0-.9 2 7.54 7.54 0 0 0-.5 2.69.61.61 0 0 0 0 .08 7.5 7.5 0 0 0 7.1 7.41h.3a7.35 7.35 0 0 0 2.91-.59c.66-.26 1.76-.76 2.9-1.3l.41.17a18.59 18.59 0 0 0 7.07 1.71 7.51 7.51 0 0 0 7.14-7.39 9 9 0 0 0-.49-2.72 11.18 11.18 0 0 0-.94-2.05zm-5.72 10.39a15.1 15.1 0 0 1-5.28-1.14l.34-.17 1-.46a.86.86 0 0 0 .18-.14l1-.51.94-.48a.89.89 0 0 0-.8-1.59l-1 .48c-1.55.79-4.14 2.12-5.23 2.57l-.1.07c-.83.39-1.6.73-2.08.92a5.2 5.2 0 0 1-2.4.45h-.09a5.72 5.72 0 0 1-5.42-5.62.35.35 0 0 0 0-.07 5.82 5.82 0 0 1 .4-2.09 9.27 9.27 0 0 1 .77-1.7l5.49-9.65c1.49-2.6 3.43-4 5.47-4s4 1.43 5.47 4l5.53 9.63a9.47 9.47 0 0 1 .77 1.7 7.21 7.21 0 0 1 .42 2.18 5.71 5.71 0 0 1-5.38 5.62z"></path></svg>
                                    </use>
                                </svg>
                            </span>
                    </div>
                    <span>281564 миленичиња се достапни на Petfinder</span>
                </div>
                <div class="purple-caption">
                    <span class="btn-text white">Запознај ги</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container hidden-sm hidden-xs">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-centered margin-top">Мачиња за посвојување во Скопје</h2>
        </div>
    </div>
</div>

<div class="container-fluid pet-card-container hidden-sm hidden-xs">
    <div class="row">
        @foreach($cats as $pet)
        <div class="col-md-3 pet-card-element u-vr10x">
            <div class="pet-card white-bg petFirstLayer">
                <div class="media">
                    <a href="{{route('petID', ['id' => encrypt($pet->id)])}}">
                        <div class="media-image">
                            <img class="img-responsive card-image" src="{{asset('storage/'.$pet->uploads()->first()->filename)}}">
                        </div>
                    </a>
                    <div class="white-clip">
                    </div>
                </div>
                <div class="card-caption">
                    <p class="pet-name">{{$pet->name}}</p>
                </div>
            </div>
            <div class="pet-card white-bg petSecondLayer">
                <a href="{{route('petID', ['id' => encrypt($pet->id)])}}">
                    <div class="media">
                        <div class="media-image">
                            <img class="img-responsive card-image" src="{{asset('storage/photos/'.$pet->uploads()->first()->resized_name)}}">
                        </div>
                    </div>
                </a>
                <div class="white-circle click-share">
                    <i class="icon-share fas fa-share icon-share-close"></i>
                </div>
                <div class="card-caption">
                    <p class="second-layer-pet-name">{{$pet->name}}</p>
                    <ul class="list-unstyled pet-details detailsList">
                        <li>@if($pet->type == 'cat') Маче @elseif($pet->type == 'dog') Куче @endif
                        </li>
                        <li>
                            @if($pet->age == 'young') Младо @elseif($pet->age == 'adult') Возрасно @elseif($pet->age == 'old') Старо @endif &#9900
                            @if($pet->gender == 'm') Машко @else Женско @endif
                        </li>
                        <li>{{$pet->city->name}}, Македонија</li>
                    </ul>
                    <p class="share-pet-name">
                        @if ($pet->gender == 'm') Сподели го @else Сподели ја @endif {{$pet->name}}
                    </p>
                    <ul class="list-unstyled list-inline shareList">
                        <li><i class="fab fa-facebook-f"></i></li>
                        <li><i class="fab fa-twitter"></i></li>
                        <li><i class="fab fa-pinterest"></i></li>
                        <li><i class="fas fa-envelope"></i></li>
                        <li><i class="fas fa-link"></i></li>
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
        <div class="col-md-3">
            <div class="pet-card purple-card">
                <div class="purple-media">
                    <div class="paw-image">
                        <span class="icon icon_xxl m-icon_colorWhite">
                                <svg role="img" focusable="false" aria-hidden="true">
                                    <use xlink:href="#icon-strokedPaw">
                                        <svg id="icon-strokedPaw" viewBox="0 0 42.26 40.83" width="100%" height="100%"><title>icon-paw</title><path d="M10.42 17.57c0-3.51-2.34-6.36-5.21-6.36S0 14.06 0 17.57s2.34 6.35 5.21 6.35 5.21-2.85 5.21-6.35zm-5.21 4.57c-1.9 0-3.44-2.05-3.44-4.58s1.54-4.58 3.44-4.58 3.42 2.06 3.42 4.59-1.52 4.57-3.42 4.57zM42.26 17.56c0-3.5-2.34-6.35-5.21-6.35s-5.21 2.85-5.21 6.36a.89.89 0 1 0 1.77 0c0-2.52 1.54-4.58 3.44-4.58s3.44 2.05 3.44 4.58c0 2.52-1.54 4.57-3.44 4.57a.89.89 0 0 0 0 1.78c2.87 0 5.21-2.85 5.21-6.36zM27.63 12.72c2.87 0 5.21-2.85 5.21-6.36S30.49 0 27.63 0s-5.21 2.85-5.21 6.36 2.32 6.36 5.21 6.36zm0-10.94c1.9 0 3.44 2.05 3.44 4.58s-1.56 4.58-3.44 4.58-3.44-2.05-3.44-4.58 1.53-4.58 3.44-4.58zM14.63 12.72c2.87 0 5.21-2.85 5.21-6.36S17.52 0 14.63 0 9.42 2.85 9.42 6.36s2.35 6.36 5.21 6.36zm0-10.94c1.9 0 3.44 2.05 3.44 4.58s-1.53 4.58-3.44 4.58-3.44-2.05-3.44-4.58 1.56-4.58 3.44-4.58zM33.63 28.65l-5.5-9.63c-1.82-3.17-4.31-4.92-7-4.92s-5.18 1.75-7 4.92l-5.5 9.62a11.12 11.12 0 0 0-.9 2 7.54 7.54 0 0 0-.5 2.69.61.61 0 0 0 0 .08 7.5 7.5 0 0 0 7.1 7.41h.3a7.35 7.35 0 0 0 2.91-.59c.66-.26 1.76-.76 2.9-1.3l.41.17a18.59 18.59 0 0 0 7.07 1.71 7.51 7.51 0 0 0 7.14-7.39 9 9 0 0 0-.49-2.72 11.18 11.18 0 0 0-.94-2.05zm-5.72 10.39a15.1 15.1 0 0 1-5.28-1.14l.34-.17 1-.46a.86.86 0 0 0 .18-.14l1-.51.94-.48a.89.89 0 0 0-.8-1.59l-1 .48c-1.55.79-4.14 2.12-5.23 2.57l-.1.07c-.83.39-1.6.73-2.08.92a5.2 5.2 0 0 1-2.4.45h-.09a5.72 5.72 0 0 1-5.42-5.62.35.35 0 0 0 0-.07 5.82 5.82 0 0 1 .4-2.09 9.27 9.27 0 0 1 .77-1.7l5.49-9.65c1.49-2.6 3.43-4 5.47-4s4 1.43 5.47 4l5.53 9.63a9.47 9.47 0 0 1 .77 1.7 7.21 7.21 0 0 1 .42 2.18 5.71 5.71 0 0 1-5.38 5.62z"></path></svg>
                                    </use>
                                </svg>
                            </span>
                    </div>
                    <span>281564 миленичиња се достапни на Petfinder</span>
                </div>
                <div class="purple-caption">
                    <span class="btn-text white">Запознај ги</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row white-bg-1">

        {{-- Carosel for mobile and tablet --}}

        <div class="cover2-holder hidden-sm hidden-md hidden-lg">
            <img class="cover2" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/06/hero-dog-checklist.jpg">
        </div>
        <div class="hidden-sm hidden-md hidden-lg col-sm-12">
            <h2 class="learn-more-heading text-center">Планираш да вдомиш милениче?</h2>
        </div>
        <div class="hidden-sm hidden-md hidden-lg col-sm-12">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="col-xs-12 col-sm-4 col-md-4 learn-more-card">
                            <div class="blue-image">
                                <img class="blue-icon" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-18.png">
                            </div>
                            <div class="learn-more-caption">
                                <p class="learn-more-caption-lg">Вовед за нови посвојувачи</p>
                                <span>
                            Прилагоди се најлесно што може.
                        </span><br>
                                <div class="btnLearnMoreMobile">
                                    Дознај повеќе
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="col-sm-4 col-md-4 learn-more-card">
                            <div class="blue-image">
                                <img class="blue-icon" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-17.png">
                            </div>
                            <div class="learn-more-caption">
                                <p class="learn-more-caption-lg">Најди го вистинското милениче</p>
                                <span>
                            Донеси правилна одлука.
                        </span><br>
                                <div class="btnLearnMoreMobile">
                                    Дознај повеќе
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="col-xs-12 col-sm-4 col-md-4 learn-more-card">
                            <div class="blue-image">
                                <img class="blue-icon" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-8.png">
                            </div>
                            <div class="learn-more-caption">
                                <p class="learn-more-caption-lg">За Brainster</p>
                                <span>
                            Научи како да правиш вакви проекти.
                        </span><br>
                                <div class="btnLearnMoreMobile">
                                    Дознај повеќе
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
            </div>
        </div>


        {{-- Regular --}}

        <div class="cover2-holder hidden-xs">
            <img class="cover2" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/06/hero-dog-checklist.jpg">
        </div>
        <h2 class="learn-more-heading text-center hidden-xs">Планираш да вдомиш милениче?</h2>
    </div>
</div>
<div class="container-fluid hidden-xs">
    <div class="row  white-bg">
        <div class="col-xs-12 col-sm-4 col-md-4 learn-more-card">
            <div class="blue-image">
                <img class="blue-icon" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-18.png">
            </div>
            <div class="learn-more-caption">
                <p class="learn-more-caption-lg">Вовед за нови посвојувачи</p>
                <span>
                        Прилагоди се најлесно што може.
                    </span><br>
                <button class="btn-clear btn-blue">
                        Дознај повеќе
                    </button>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 learn-more-card">
            <div class="blue-image">
                <img class="blue-icon" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-17.png">
            </div>
            <div class="learn-more-caption">
                <p class="learn-more-caption-lg">Најди го вистинското милениче</p>
                <span>
                        Донеси правилна одлука.
                    </span><br>
                <button class="btn-clear btn-blue">
                        Дознај повеќе
                    </button>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 learn-more-card">
            <div class="blue-image">
                <img class="blue-icon" src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-8.png">
            </div>
            <div class="learn-more-caption">
                <p class="learn-more-caption-lg">За Brainster</p>
                <span>
                        Научи како да правиш вакви проекти.
                    </span><br>
                <button class="btn-clear btn-blue">
                        Дознај повеќе
                    </button>
            </div>
        </div>
    </div>
</div>
@endsection