@extends('master') 
@section('content')
<div class="container-fluid">
    <div class="row">
            <div class="carousel2 carousel slide" id="myCarousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    @foreach($pet->uploads as $item)
                        <li data-target="#PetPicturesCarousel" data-slide-to="{{ $loop->index }}"
                            class="{{ $loop->first ? ' active' : '' }}"></li>
                    @endforeach
                </ol>

                <div class="carousel-inner">
                    @foreach($pet->uploads as $item)
                        <div class="item {{ $loop->first ? ' active' : '' }}">
                            <div class="col-md-4">
                         
                                <img src="{{asset('storage/'.$item->filename)}}" class="img-responsive" style="height: 450px; width: 100%;">
                               
                            </div>
                        </div>
                    @endforeach
                </div>
                     
                <!-- Controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <i class="glyphicon glyphicon-chevron-right"></i>
                </a>
            </div>
    </div>
</div>
   


    
       
    <div class="container-fluid">
        <div class="row backgroundPetInfo">
            <div class="col-md-6 col-md-offset-1 petInfo">
                <div class="col-md-12 purple">
                    <h1>{{$pet->name}}</h1>
                </div>
                <div class="col-md-12 purple">
                    <p>{{$pet->city->name}} 
                </div>
                <div class="col-md-12 line-purple"></div>
                <div class="col-md-12">
                    <h3>За миленикот</h3>
                    <ul>
                        <li> @if($pet->type == 'cat')
                                &#9900 Маче
                                @elseif($pet->type == 'dog')
                                &#9900 Куче
                                @endif</li>
                        <li> @if($pet->age == 'young') 
                                &#9900 Младо
                                @elseif($pet->age == 'adult')
                                &#9900 Возрасно
                                @elseif($pet->age == 'old')
                                &#9900 Старо
                                @endif </li>
                        <li>@if ($pet->gender == 'm')
                                &#9900 Машко
                                @else
                                &#9900 Женско
                                @endif </li>
                        <li>@if ($pet->size == 'sm')
                                &#9900 Мал раст
                                @elseif ($pet->size == 'md')
                                &#9900 Среден раст
                                @else
                                &#9900 Голем раст
                                @endif </li>
                    </ul>

                </div>
            </div>
            <div class="col-md-3 col-md-offset-1 AskAboutPet text-center">
                <div class="col-md-12">
                    <a href="#EmailForm"><button type="button"  class="askBtn">Информирај се за {{$pet->name}}</button></a>
                </div>
                <div class="col-md-6 sharePrint share"><i class="fas fa-share"></i></i>Сподели</div>
                <div class="col-md-6 sharePrint print"><i class="fas fa-print"></i>Испринтај</div>
            </div>
            <div class="col-md-6 col-md-offset-1 petInfo margin-top-2">
                <h3> @if ($pet->gender == 'm')
                        Запознај го
                        @else
                        Запознај ја 
                        @endif{{$pet->name}}</h3>
                <p>{{$pet->description}}</p>

            </div>
        </div>
    </div>
    <div class="container-fluid" id="EmailForm">
        <div class="row AskForm" >
            <div class="mailHeader">
                <div class="col-md-1 col-md-offset-1 roundImage"><img src="{{asset('storage/photos/'.$pet->uploads()->first()->resized_name)}}"></div>
                <div class="col-md-9 mailHeading">
                    <span class="mailH2">Прашај за {{$pet->name}}</span>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 line"></div>
            <div class="col-md-10 col-md-offset-1">
                <h5>До:</h5>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <p>{{$pet->user->first_name}} {{$pet->user->last_name}}</p>
                
            </div>
        </div>
        <div class="row margin-top-2 row-container-margin">
            <div class="col-md-12">
                @guest
                <p>Имаш профил? Најави се</p>
                @endguest
            </div>
        </div>
        <div class="row row-container-margin mail-content-div">
            <div class="col-md-6">
                @guest
                <span class="btn-text">Или прати порака како гостин</span>
                @else
                <span class="btn-text">Прати порака</span>
                @endguest
            
                <div class="row sending-email-inputs">
                    
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <input type="hidden" name="pet_owner_id" value="{{$pet->user->id}}">
                        <input type='hidden' name="pet_id" value="{{$pet->id}}">
                        <input type="text" class="login-input{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" placeholder="Име"
                            value={{\Auth::user() ? \Auth::user()->first_name : '' }}> @if($errors->has('first_name'))
                        <span class="red pull-right">{{$errors->first('first_name')}}</span> @endif
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <input type="text" class="login-input{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" placeholder="Презиме"
                            value={{\Auth::user() ? \Auth::user()->last_name : '' }}> @if($errors->has('last_name'))
                        <span class="red pull-right">{{$errors->first('last_name')}}</span> @endif
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <input type="text" class="login-input{{ $errors->has('telephone') ? ' is-invalid' : '' }}" name="telephone" placeholder="Телефонски број"
                            value="{{\Auth::user() ? \Auth::user()->telephone : ''}}"> @if($errors->has('telephone'))
                        <span class="red pull-right">{{$errors->first('telephone')}}</span> @endif
                    </div>
                    <div class="col-md-6">
                        <input type="email" name="email" class="login-input{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Електронска пошта"
                            value="{{\Auth::user() ? \Auth::user()->email : ''}}"> @if($errors->has('email'))
                        <span class="red pull-right">{{$errors->first('email')}}</span> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        @guest
                        <select class="login-input" name="city_id">
                            <option selected disabled>Град</option>
                            @foreach (\App\City::all() as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                        @else
                        <select class="login-input{{ $errors->has('city_id') ? ' is-invalid' : '' }}" name="city_id">
                            @foreach (\App\City::all() as $city)
                                <option {{ $city->id == \Auth::user()->city_id ? 'selected' : '' }} value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select> @if($errors->has('city_id'))
                        <span class="red pull-right">{{$errors->first('city_id')}}</span> @endif
                        @endguest
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <span class="btn-text">Твојата порака (Може да содржи до 5000 карактери)</span>
                        <textarea class="login-input-textarea" id="message" name="message" rows="5" cols="20"></textarea>                            
                        @if($errors->has('message'))
                        <span class="red pull-right">{{$errors->first('message')}}</span> @endif
                    </div>
                    <div class="col-md-12">
                        <input type="submit" class="login-register-btn" id="inquire" value="Прати порака">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection