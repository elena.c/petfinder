<!DOCTYPE html>

<html>

<head>
    @include('includes.head')
</head>

<body>
    @include('includes.navbar')

<div class="filter-cards-container">
    <div class="filter-cards-list-holder">
        <ul class="filter-cards-list">
            <li class="filter-span">
                <div class="filter-form">
                    <div class="filter-form-text"></div>
                    <span>
                       <i class="fas fa-times filter-form-close"></i>
                   </span>
                </div>
            </li>
        </ul>
    </div>
</div>
<form class="formFilter">
<div class="container-fluid u-vr5x" id="filtersContainer">
    <div class="row t-vr5x">
        <div class="col-md-1">
            <label for="type">Вид
            </label>
            <select name="type" id="type" class="login-input">
                <option value=-1 hidden>Сите</option>
                <option value="all">Сите</option>
                <option value="cat">Маче</option>
                <option value="dog">Куче</option>
            </select>
        </div>
        <div class="col-md-2">
            <label for="city_id">Град
            </label>
            <select name="city_id" id="city_id" class="login-input">
                <option value=-1 hidden>Одбери...</option>
                <option data-value="Било каде">Било каде</option>
                @foreach (\App\City::all() as $city)
                    <option value="{{$city->id}}" data-value="{{$city->name}}">{{$city->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            <label for="age">Возраст</label><br>
            <select class="login-input" id="age" name="age">
                <option value=-1 hidden>Одбери...</option>
                <option value="young" data-value="Младо">Младо</option>
                <option value="adult" data-value="Возрасно">Возрасно</option>
                <option value="old" data-value="Старо">Старо</option>
            </select>
        </div>
        <div class="col-md-2">
            <label for="size">Раст</label>
            <select class="login-input" id="size" name="size">
                <option value=-1 hidden>Одбери...</option>
                <option value="sm" data-value="Мал раст">Мал раст</option>
                <option value="md" data-value="Среден раст">Среден раст</option>
                <option value="lg" data-value="Голем раст">Голем раст</option>
            </select>
        </div>
        <div class="col-md-2">
            <label for="gender">Пол</label>
            <select class="login-input" id="gender" name="gender">
                <option value=-1 hidden>Одбери...</option>
                <option value="m" data-value="Машко">Машки</option>
                <option value="f" data-value="Женско">Женски</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="good_with">Добро се согласува со</label><br>
            <div class="login-input less-padding login-input-list">
                <input class="checkboxes" type="checkbox"  name="good_with[]" value=1 data-value="Мачки">Мачки
                <input class="checkboxes" type="checkbox"  name="good_with[]" value=2 data-value="Кучиња">Кучиња
                <input class="checkboxes" type="checkbox"  name="good_with[]" value=3 data-value="Деца">Деца
            </div>
        </div>
    </div>
</div>
</form>
<div class="container-fluid pet-card-container">
    <div class="row all-pets">
        @foreach ($pets as $pet)  
            <div class="col-md-3 pet-card-element u-vr10x">
                <div class="pet-card white-bg petFirstLayer">
                    <div class="media">
                        <a href="{{route('petID', ['id' => encrypt($pet->id)])}}">
                            <div class="media-image">  
                                <img class="img-responsive card-image" src="{{asset('storage/'.$pet->uploads()->first()->filename)}}">
                            </div>
                        </a>
                        <div class="white-clip">
                        </div>
                    </div>
                    <div class="card-caption">
                        <p class="pet-name">{{$pet->name}}</p>
                    </div>
                </div>
                <div class="pet-card white-bg petSecondLayer">
                    <a href="{{route('petID', ['id' => encrypt($pet->id)])}}">
                        <div class="media">
                            <div class="media-image">
                                <img class="img-responsive card-image" src="{{asset('storage/photos/'.$pet->uploads()->first()->resized_name)}}">
                            </div>          
                        </div>
                    </a>
                    <div class="white-circle click-share">
                        <i class="icon-share fas fa-share icon-share-close"></i>
                    </div>
                    <div class="card-caption">
                        
                        <p class="second-layer-pet-name">{{$pet->name}}</p>
                        <ul class="list-unstyled pet-details detailsList">
                            <li>@if($pet->type == 'cat')
                                Маче
                                @elseif($pet->type == 'dog')
                                Куче
                                @endif
                            </li>
                            <li>
                                @if($pet->age == 'young') 
                                Младо
                                @elseif($pet->age == 'adult')
                                Возрасно
                                @elseif($pet->age == 'old')
                                Старо
                                @endif                                   
                            &#9900 
                                @if ($pet->gender == 'm')
                                    Машко
                                @else
                                    Женско
                                @endif
                            </li>
                            <li>{{$pet->city->name}}, Македонија</li>
                        </ul>
                        <p class="share-pet-name">
                            @if ($pet->gender == 'm')
                            Сподели го 
                            @else
                            Сподели ја 
                            @endif
                            {{$pet->name}}</p>
                        <ul class="list-unstyled list-inline shareList">
                            <li><i class="fab fa-facebook-f"></i></li>
                            <li><i class="fab fa-twitter"></i></li>
                            <li><i class="fab fa-pinterest"></i></li>
                            <li><i class="fas fa-envelope"></i></li>
                            <li><i class="fas fa-link"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

<script src="{{asset('js/customjq.js')}}"></script>
<script src="{{asset('js/petcards.js')}}"></script>
<script src="{{asset('js/filterstyle.js')}}"></script>
<script src="{{asset('js/filtering.js')}}"></script>
<script src="{{asset('js/mail.js')}}"></script>
<script src="{{asset('js/slider.js')}}"></script>

</body>



</html>