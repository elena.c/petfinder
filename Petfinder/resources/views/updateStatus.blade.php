<!DOCTYPE html>
<html>
<head>
    @include('includes.headAdmin')
</head>
<body> 

     <div class="container-fluid petsForApprove">
        <div class="row">
            <div class="table-responsive">
            <table class="table">
                            <thead class="tableHead">
                                <tr>
                                    <th>Име</th>
                                    <th>Тип</th>
                                    <th>Име на корисник</th>
                                    <th>Град</th>
                                    <th>Години</th>
                                    <th>Големина</th>
                                    <th>Пол</th>
                                    <th>Опис</th>
                                    <th>Статус</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    
                                </tr>
                            </thead>
                            <tbody class="table">
                               
                                  <tr>
                                    <td>{{ $pet->name }}</td>
                                    <td>{{ $pet->type}}</td>
                                    <td>{{ $pet->user->first_name }}</td>
                                    <td>{{ $pet->city->name }}</td>
                                    <td>{{ $pet->age }}</td>
                                    <td>{{ $pet->size }}</td>
                                    <td>{{ $pet->gender }}</td>
                                    <td>{{ $pet->description }}</td>
                                    <td>{{ $pet->approved }}</td>

                                    <td><a href="{{ route('approved',['id' => $pet->id]) }}"class="btn btn-primary">Одобри</a> </td>

                                    <td><a href="{{ route('deny',['id' => $pet->id]) }}"class="btn btn-warning">Одбиј</a> </td>

                                    <td><a href="{{ route('delete',['id' => $pet->id]) }}"class="btn btn-danger">Избриши</a> </td>

                                  </tr>  


                        </tbody>
                        </table>
                        </div>

        </div>
    </div>

</body>
</html>
