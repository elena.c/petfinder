<?php

use App\City;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\DatabaseController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\AdminController;
use App\Http\Middleware\CheckRole;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GeneralController@welcome')->name('welcome');

Auth::routes();

// Socialite Routes
Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');

Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');


Route::get('/home', 'HomeController@index')->name('home');   

Route::get('/user', 'HomeController@editProfileUser')->name('editProfileUser');

Route::post('/user', 'DatabaseController@editProfile')->name('editProfilePost');

Route::get('/user/pet', 'HomeController@editProfilePet')->name('editProfilePet');

Route::post('/user/pet', 'DatabaseController@savePet')->name('savePet');

//Image Upload routes 

Route::post('/images-save', 'UploadImagesController@uploadSubmit')->name('addPhotosPet');

Route::get('/pets', 'HomeController@listPets')->name('listPets');

Route::get('/filter', 'PetsFilterController@filter')->name('filter');

Route::get('/pet', 'GeneralController@petID')->name('petID');

Route::get('/pet/details', 'GeneralController@petDetails')->name('petDetails');

Route::post('/inquirepet', 'MailController@inquirePet')->name('inquirePet');

// ADMIN 
Route::get('/adminpanel')->name('adminpanel')->uses('AdminController@showAdminPanel');

Route::get('/update/{id}')->name('update')->uses('AdminController@updateStatus');

Route::get('/approved/{id}')->name('approved')->uses('AdminController@approvePost');

Route::get('/deny/{id}')->name('deny')->uses('AdminController@deny');

Route::get('/delete/{id}')->name('delete')->uses('AdminController@delete');




